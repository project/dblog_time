<?php

namespace Drupal\dblog_time;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Manages the watchdog message deletions based on the configured settings.
 */
class DatabaseLoggingTimeManager {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    Connection $database,
    ModuleHandlerInterface $module_handler,
  ) {
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Access the manager service.
   *
   * @return \Drupal\dblog_time\DatabaseLoggingTimeManager
   *   The manager service instance.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('module_handler')
    );
  }

  /**
   * Run the cron task.
   */
  public function runCron(): void {
    // If we have a timestamp AND it calculates properly with string to time.
    $settings = $this->configFactory->get('dblog_time.settings');
    $limit_type = $settings->get('limit_type');
    $timespan = $settings->get('timespan');
    if (
      $limit_type === 'timespan'
      && $timespan
      && $timespan = strtotime($timespan)
    ) {
      $overrides = $this->getPerTypeOverrides();
      if (!empty($overrides)) {
        $this->runCronWithOverrides($timespan, $overrides);
      }
      else {
        $this->runCronWithoutOverrides($timespan);
      }
    }
    else {

      // Fallback to Drupal Core Database Logging cron.
      $this->moduleHandler->loadInclude('dblog', 'module');
      dblog_cron();
    }
  }

  /**
   * Handle running the cron where type overrides have been set.
   *
   * @param int $timespan
   *   The fallback timespan.
   * @param array $overrides
   *   The per type timespan settings keyed by type.
   */
  protected function runCronWithOverrides(int $timespan, array $overrides): void {

    // Delete anything older than the timestamp and not in the keys.
    $this->database->delete('watchdog')
      ->condition('timestamp', $timespan, '<')
      ->condition('type', array_keys($overrides), 'NOT IN')
      ->execute();

    // Per type, delete anything older than the specified timespan.
    foreach ($overrides as $type => $per_type_timespan) {
      $this->database->delete('watchdog')
        ->condition('timestamp', $per_type_timespan, '<')
        ->condition('type', $type)
        ->execute();
    }
  }

  /**
   * Handle running the cron where type overrides have not been set.
   *
   * @param int $timespan
   *   The fallback timespan.
   */
  protected function runCronWithoutOverrides(int $timespan): void {

    // Delete anything older than the timestamp.
    $this->database->delete('watchdog')
      ->condition('timestamp', $timespan, '<')
      ->execute();
  }

  /**
   * Return an array of timespan settings keyed by type.
   *
   * @return array
   *   The per type overrides.
   */
  protected function getPerTypeOverrides(): array {
    $overrides = $this->configFactory->get('dblog_time.settings')->get('timespan_per_type_override');
    if (!str_contains($overrides, '|')) {
      return [];
    }

    $parsed_overrides = [];
    $overrides = str_replace(["\r\n", "\r"], "\n", $overrides);
    foreach (explode("\n", $overrides) as $override) {
      $override = trim($override);

      // Ignore empty lines in between non-empty lines.
      if (empty($override)) {
        continue;
      }

      // Ignore if no |.
      if (!str_contains($override, '|')) {
        continue;
      }

      // Parse the line.
      [$type, $timespan] = explode('|', $override);
      $type = trim($type);
      $timespan = trim($timespan);

      // Ignore if either is empty.
      if (empty($type) || empty($timespan)) {
        continue;
      }

      // Only add if the timespan is parse-able by string to time.
      if ($timespan = strtotime($timespan)) {
        $parsed_overrides[$type] = $timespan;
      }
    }
    return $parsed_overrides;
  }

}
