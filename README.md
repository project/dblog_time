# Database Logging Time

This module is useful for projects where you wish to keep database logging messages for a set period of time. You understand the risks that setting a time limit increases the risk of having a high quantity of items in your database log.

## Features

This enhances the Database Logging Module from core by:

- Adding an index on the timestamp for performance filtering by date (eg, you can use this in the /admin/reports/dblog View)
- Providing an option to have cron delete messages by amount of time that has passed rather than by quantity of messages

## Post-Installation

Go to /admin/config/development/logging and choose 'Timespan' instead of the default 'Row limit'. Enter a timespan like '-7 days' to delete messages that are older than 7 days. This accepts anything that strtotime() accepts.