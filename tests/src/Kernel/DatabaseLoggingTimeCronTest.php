<?php

namespace Drupal\Tests\dblog_time\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test cron deletes logs correctly.
 *
 * @group dblog_time
 */
class DatabaseLoggingTimeCronTest extends KernelTestBase {

  /**
   * The modules to enable for this test.
   *
   * @var array
   */
  protected static $modules = [
    'dblog',
    'system',
    'dblog_time',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('system', ['sequences']);
    $this->installSchema('dblog', ['watchdog']);
    $this->installConfig(['dblog', 'dblog_time']);
  }

  /**
   * Test the base unified date field gets saved.
   */
  public function testWatchdog(): void {
    \Drupal::configFactory()->getEditable('dblog_time.settings')
      ->set('limit_type', 'timespan')
      ->set('timespan', '-30 minutes')
      ->set('timespan_per_type_override', '
        test1|-5 minutes
        test2|-10 minutes
      ')
      ->save();

    // Add sample logger messages.
    foreach (range(1, 3) as $logger_number) {
      $logger = \Drupal::logger('test' . $logger_number);

      // Set messages to 20, 8, and 1 minute ago.
      // Test1 is set to 5 minutes so only 1 minute ago should get deleted.
      // Test2 is set to 10 minutes so 1 and 8 minutes ago should get deleted.
      // Test3 is set to the fallback 30 minutes, so all should get deleted.
      foreach ([20, 8, 1] as $key => $minutes_ago) {
        $message = 'Test ' . $logger_number . ': Message ' . ($key + 1);
        $logger->notice($message, [
          'timestamp' => strtotime('-' . $minutes_ago . ' minutes'),
        ]);
      }
    }

    // Run the cron job.
    dblog_time_cron();

    $database = \Drupal::database();
    $query = $database->select('watchdog', 'w');
    $query->addField('w', 'message');
    $messages = $query->execute()->fetchCol();

    $this->assertIsArray($messages);

    // Test 1 messages should have messages 1 & 2 deleted.
    $this->assertNotContains('Test 1: Message 1', $messages);
    $this->assertNotContains('Test 1: Message 2', $messages);
    $this->assertContains('Test 1: Message 3', $messages);

    // Test 2 messages should have message 1 deleted.
    $this->assertNotContains('Test 2: Message 1', $messages);
    $this->assertContains('Test 2: Message 2', $messages);
    $this->assertContains('Test 2: Message 3', $messages);

    // Test 3 messages should have all messages deleted.
    $this->assertContains('Test 3: Message 1', $messages);
    $this->assertContains('Test 3: Message 2', $messages);
    $this->assertContains('Test 3: Message 3', $messages);
  }

}
